package com.mc4.vertxlearning.httpserver;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class HttpServerVerticle extends AbstractVerticle {

    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(new HttpServerVerticle());
        vertx.deployVerticle(new ProcessingVerticle());
    }

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        HttpServer server = vertx.createHttpServer();

        Router router = Router.router(vertx);
        router.get("/provider/:resourceId/")
                .handler(this::ingestionHandler)
                .failureHandler(this::failureHandler);

        server.requestHandler(router::accept).listen(1234, ar -> {
            if (ar.succeeded()) {
                startFuture.complete();
            } else {
                startFuture.fail(ar.cause());
            }
        });

    }

    private void failureHandler(RoutingContext routingContext) {
        routingContext
                .response()
                .setStatusCode(500)
                .end("Exception handled");
    }

    private void ingestionHandler(RoutingContext routingContext) {
        String requestedId = routingContext.request().getParam("resourceId");
        System.out.println(requestedId);
        vertx.eventBus().send("test-topic", requestedId, response -> {
            if (response.succeeded()) {
                routingContext.response().end(response.result().body().toString());

            } else {
                System.out.println(response.cause());
                ReplyException cause = (ReplyException) response.cause();
                String failMessage = cause.getMessage();
                System.out.println(failMessage);
                int failCode = cause.failureCode();
                System.out.println(failCode);
                routingContext.fail(new ExampleException());
            }
        });
    }
}

