package com.mc4.vertxlearning.httpserver;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.Message;

public class ProcessingVerticle extends AbstractVerticle {
    @Override
    public void start(Future<Void> startFuture) throws Exception {
        vertx.eventBus().consumer("test-topic", this::processMessage);
        startFuture.complete();
    }

    private void processMessage(Message<String> message) {
        if (message.body().contains("error")) {
            message.fail(-1, new ExampleException("New exception").getMessage());
        } else {
            message.reply(message.body().toUpperCase());
        }
    }
}
