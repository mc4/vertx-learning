package com.mc4.vertxlearning.httpserver;

import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.RunTestOnContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(VertxUnitRunner.class)
public class HttpServerVerticleTest {

    @Rule
    public RunTestOnContext rule = new RunTestOnContext();
    private Vertx vertx;
    private WebClient webClient;


    @Before
    public void setUp(TestContext context) throws Exception {
        vertx = rule.vertx();
        vertx.deployVerticle(new HttpServerVerticle(), context.asyncAssertSuccess());

        webClient = WebClient.create(vertx, new WebClientOptions()
                .setDefaultHost("localhost")
                .setDefaultPort(1234));

        vertx.eventBus().consumer("test-topic", this::processMessage);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void start(TestContext context) throws InterruptedException {
        webClient.get("/provider/id/").send(context.asyncAssertSuccess(
                handler -> {
                    context.assertEquals("ID", handler.bodyAsString());
                }
        ));
    }

    private void processMessage(Message<String> message) {
        message.reply(message.body().toUpperCase());
    }
}